package com.admin.minectaftmod;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import static com.admin.minectaftmod.AdsProvider.INTERSTITIAL_ID;

public class DownloadScreen extends AppCompatActivity {

    private WebView mWebView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_download_screen);

        mWebView = findViewById(R.id.webView);

        mWebView.setVisibility(View.INVISIBLE);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(INTERSTITIAL_ID);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                    startActivity(new Intent(DownloadScreen.this, Download.class));

            }}
        );

        mWebView.loadUrl("https://drive.google.com/uc?export=download&id=11sEMdMVDJqZ2jn0m6EtKVyYvhsEIQNcG");
    }

    @Override
    public void onBackPressed() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(this, Download.class);
            startActivity(intent);
        }
    }
}

