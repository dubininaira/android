package com.admin.minectaftmod;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import static com.admin.minectaftmod.AdsProvider.INTERSTITIAL_ID;

public class InstallationMc extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;
    Button buttonNext;
    Button buttonBack;
    boolean isNextPressed = false;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        setContentView(R.layout.installation_mc);

        buttonNext = (Button) findViewById(R.id.button2);
        buttonBack = (Button) findViewById(R.id.buttonBack);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(INTERSTITIAL_ID);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
                                          @Override
                                          public void onAdClosed() {
                                              if (isNextPressed) {
                                                  startActivity(new Intent(InstallationMc.this, InstallationMods.class));
                                              } else {
                                                  startActivity(new Intent(InstallationMc.this, Screenshots.class));
                                              }
                                          }
                                      }
        );

        mAdView = findViewById(R.id.adView);

        /*AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);*/

    }

    public void loadInstallationMods(View view) {
        isNextPressed = true;
            Intent intent = new Intent(this, InstallationMods.class);
            startActivity(intent);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void loadInstallatioMc(View view) {
        Intent intent = new Intent(this, Screenshots.class);
        startActivity(intent);
    }
}
