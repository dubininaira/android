package com.admin.minectaftmod;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import static com.admin.minectaftmod.AdsProvider.APP_ID;
import static com.admin.minectaftmod.AdsProvider.INTERSTITIAL_ID;


public class Main extends AppCompatActivity {

    boolean isNextPressed = false;
    private static final String TAG = "Main";
    private Button buttonToDescription;
    private AdView mAdView;

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.main);

        buttonToDescription = (Button) findViewById(R.id.button2);

        MobileAds.initialize(this, APP_ID);

        prepareAd();


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                if (isNextPressed) {
                    startActivity(new Intent(Main.this, Description.class));
                } else{
                    startActivity(new Intent(Main.this, Download.class));
                }
            }}
        );

        mAdView = findViewById(R.id.adViewMob);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    public void loadDescription(View view) {
        try {
            isNextPressed = true;

            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
                Intent intent = new Intent(this, Description.class);
                startActivity(intent);
            }
        } catch (Exception e){
            Intent intent = new Intent(this, Description.class);
            startActivity(intent);
        }
    }
    public void prepareAd() {

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(INTERSTITIAL_ID);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }
}
