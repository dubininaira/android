package com.admin.minectaftmod;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import static com.admin.minectaftmod.AdsProvider.APP_ID;
import static com.admin.minectaftmod.AdsProvider.INTERSTITIAL_ID;

public class Download extends AppCompatActivity implements View.OnClickListener {
    private static Button downloadZip;
    private boolean isDownloadPressed = false;
    private InterstitialAd mInterstitialAd;
    TextView log;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        setContentView(R.layout.download);

        downloadZip = findViewById(R.id.downloadZip);
        downloadZip.setEnabled(false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                downloadZip.setEnabled(true);
            }
        }, 4000);

        log = findViewById(R.id.log);

        MobileAds.initialize(this, APP_ID);

        initViews();
        setListeners();

        mInterstitialAd = new InterstitialAd(this);

        mInterstitialAd.setAdUnitId(INTERSTITIAL_ID);

//        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
                                          @Override
                                          public void onAdClosed() {
                                              if (isDownloadPressed) {
                                                  startActivity(new Intent(Download.this, DownloadScreen.class));
                                              } else {
                                                  startActivity(new Intent(Download.this, InstallationMods.class));
                                              }
                                          }
                                      }
        );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void loadInstallationMods(View view) {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(this, InstallationMods.class);
            startActivity(intent);
        }
    }


    private void initViews() {
        downloadZip = (Button) findViewById(R.id.downloadZip);
    }

    private void setListeners() {
        downloadZip.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        isDownloadPressed = true;

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(this, DownloadScreen.class);
            startActivity(intent);
        }
    }
}