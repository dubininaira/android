package com.admin.minectaftmod;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import static com.admin.minectaftmod.AdsProvider.INTERSTITIAL_ID;

public class InstallationMods extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;
    boolean isNextPressed = false;
    boolean isOnBackPressed = false;
    Button buttonNext;
    Button buttonBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        setContentView(R.layout.installation_mods);

        buttonNext = (Button) findViewById(R.id.button2);
        buttonBack = (Button) findViewById(R.id.buttonBack);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(INTERSTITIAL_ID);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                if (isNextPressed) {
                    startActivity(new Intent(InstallationMods.this, Download.class));
                } else{
                    startActivity(new Intent(InstallationMods.this, InstallationMc.class));
                }
            }}
        );
}

    public void loadDownload(View view) {
        isNextPressed = true;
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(this, Download.class);
            startActivity(intent);
        }
    }


    @Override
    public void onBackPressed() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    public void loadInstallatioMods(View view) {
        isOnBackPressed = true;
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(this, InstallationMc.class);
            startActivity(intent);
        }
    }
}
