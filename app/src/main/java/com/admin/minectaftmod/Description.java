package com.admin.minectaftmod;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import static com.admin.minectaftmod.AdsProvider.INTERSTITIAL_ID;

public class Description extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;
    Button buttonNext;
    Button buttomBack;
    private AdView mAdView;
    boolean isNextPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        setContentView(R.layout.description);

        buttonNext = (Button) findViewById(R.id.button3);
        buttomBack = (Button) findViewById(R.id.buttonBack);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(INTERSTITIAL_ID);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                if (isNextPressed) {
                    startActivity(new Intent(Description.this, Screenshots.class));
                } else{
                    startActivity(new Intent(Description.this, Main.class));
                    //              mInterstitialAd.loadAd(new AdRequest.Builder().build());
                }
            }}


        );

        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//
//        mAdView.loadAd(adRequest);
    }

    public void loadScreenshots(View view) {
        isNextPressed = true;
            Intent intent = new Intent(this, Screenshots.class);
            startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void loadMain(View view) {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(this, Main.class);
            startActivity(intent);
        }
    }
}
