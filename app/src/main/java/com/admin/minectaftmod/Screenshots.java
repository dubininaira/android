package com.admin.minectaftmod;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import static com.admin.minectaftmod.AdsProvider.INTERSTITIAL_ID;

public class Screenshots extends AppCompatActivity {
    Button buttonNext;
    boolean isNextPressed = false;
    Button buttonBack;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        setContentView(R.layout.screenshots);


        buttonNext = (Button) findViewById(R.id.button2);
        buttonBack = (Button) findViewById(R.id.buttonBack);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(INTERSTITIAL_ID);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                if (isNextPressed) {
                    startActivity(new Intent(Screenshots.this, InstallationMc.class));
                } else{
                    startActivity(new Intent(Screenshots.this, Description.class));
                    //              mInterstitialAd.loadAd(new AdRequest.Builder().build());
                }
            }
            /*@Override
            public void onAdLoaded (){
                buttonBack.setActivated(true);
            }*/}
        );

    }

    public void loadInstallationMC(View view) {
        isNextPressed = true;
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(this, InstallationMc.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    public void loadDescription(View view) {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(this, Description.class);
            startActivity(intent);
        }
    }
}